﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Contract;

namespace Lab4.Component2
{
    public class SuperMyjnia : AbstractComponent, IPrad, IMyjnia
    {
        private IWoda woda;
        private ISzczoty szczoty;

        public SuperMyjnia()
        {

            this.RegisterProvidedInterface(typeof(IMyjnia), this);
            this.RegisterRequiredInterface(typeof(IWoda));
            this.RegisterRequiredInterface(typeof(ISzczoty));
        }

        void IPrad.wlacz_prad()
        {
            Console.WriteLine("Super myjnia prad - on");

        }

        void IPrad.wylacz_prad()
        {
            Console.WriteLine("Super myjnia prad - off");
        }

        public void Start()
        {
            Console.WriteLine("super myjnia startuje");
            woda.plyn();
            szczoty.szczotkuj();
        }

        public void Stop()
        {
            Console.WriteLine("super myjnia nie myje");
            woda.nie_plyn();
            szczoty.nie_szczotkuj();
        }

        public override void InjectInterface(Type type, object impl)
        {
            if (impl is IWoda)
            {
                this.woda = impl as IWoda;
            }
            else if (impl is ISzczoty)
            {
                this.szczoty = impl as ISzczoty;
            }
        }
    }
}
