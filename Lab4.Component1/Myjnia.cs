﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Contract;

namespace Lab4.Component1
{
    public class Myjnia : AbstractComponent, IMyjnia 
    {
        private IPrad prad;
        private IWoda woda;
        private ISzczoty szczoty;
       
        public Myjnia()
        {
            this.woda = new Woda();
            this.prad = new Prad();
            this.szczoty = new Szczoty();

            this.RegisterRequiredInterface(typeof(IMyjnia));
            this.RegisterProvidedInterface(typeof(IWoda), woda);
            this.RegisterProvidedInterface(typeof(ISzczoty), szczoty);

        }


        public void Start()
        {
            Console.WriteLine("myjnia startuje"); ;
        }

        public void Stop()
        {
            Console.WriteLine("myjnia nie myje"); ;
        }

        public static IMyjnia GetObject(object impl)
        {
            return new Myjnia();
        }

        public override void InjectInterface(Type type, object impl)
        {
            if (type is IWoda)
            {
                this.woda = impl as IWoda;
            }
            else if (type is ISzczoty)
            {
                this.szczoty = impl as ISzczoty;
            }
        }
    }
}
