﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Contract;

namespace Lab4.Component1
{
    class Prad : IPrad
    {

        public void wlacz_prad()
        {
            Console.WriteLine("prad - on");
        }

        public void wylacz_prad()
        {
            Console.WriteLine("prad - off");
        }
    }
}
