﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Contract;

namespace Lab4.Component1
{
    class Woda : IWoda
    {

        public void plyn()
        {
            Console.WriteLine("woda plynie"); 
        }

        public void nie_plyn()
        {
            Console.WriteLine("woda nie plynie"); 
        }
    }
}
