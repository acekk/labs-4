﻿using System;
using Lab4.Component1;
using Lab4.Component2;
using Lab4.Contract;
using ComponentFramework;
using Lab4.Component2B;

namespace Lab4.Main
{
    public struct LabDescriptor
    {
        #region definitions

        public delegate object GetInstance(object component2);
        public delegate void RegisterComponentInContainer(object container, object component);
        public delegate bool AreDependenciesResolved(object container);

        #endregion

        #region P1

        public static Type Component1 = typeof(Myjnia);
        public static Type Component2 = typeof(SuperMyjnia);

        public static Type RequiredInterface = typeof(IMyjnia);

        public static GetInstance GetInstanceOfRequiredInterface = Myjnia.GetObject;
        
        #endregion

        #region P2

        public static Type Container = typeof(Container);

        public static RegisterComponentInContainer RegisterComponent = (kontener, component) => { ((ComponentFramework.IContainer)(kontener)).RegisterComponent((IComponent)component); };

        public static AreDependenciesResolved ResolvedDependencied = (kontener) => { return ((ComponentFramework.IContainer)(kontener)).DependenciesResolved; };

        #endregion

        #region P3

        public static Type Component2B = typeof(WypasMyjnia);

        #endregion
    }
}
