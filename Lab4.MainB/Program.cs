﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab4.Component1;
using Lab4.Component2B;
using Lab4.Contract;
using ComponentFramework;

namespace Lab4.MainB
{
    class Program
    {
        static void Main(string[] args)
        {
            Container kontener = new Container();

            IMyjnia module = new Myjnia();
            IMyjnia module2 = new WypasMyjnia();

            kontener.RegisterComponent(module as IComponent);


            kontener.RegisterComponent(module2 as IComponent);

            module.Start();
            module2.Start();

            module.Stop();
            module2.Stop();

            Console.ReadKey();
        }
    }
}
